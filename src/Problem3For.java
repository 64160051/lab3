import java.util.Scanner;

public class Problem3For {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int firstNum;
        int secondNum;
        System.out.print("Please input first number: ");
        firstNum = sc.nextInt();
        System.out.print("Please input second number: ");
        secondNum = sc.nextInt();
        if(firstNum>secondNum){
            System.out.println("Error");
        }else {
            for(int i=firstNum; i<=secondNum; i++){
                System.out.print(i + " ");
            }
        }
    }
}
